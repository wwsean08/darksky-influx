#!/usr/bin/env bash
# file to save weather data to
OFILE="weather.data"

# remove any old weather.data files
rm ${OFILE}

wget -O $OFILE https://api.darksky.net/forecast/${API_KEY}/${LOCATION}?exclude=minutely,hourly,daily,alerts,flags

TEMP=$(jq -c '.currently.temperature' ${OFILE})
FEELSLIKE=$(jq -c '.currently.apparentTemperature' ${OFILE})
# Need to multiply by 100 to turn 0.7 to 70%
HUMIDITY=$(jq -c '.currently.humidity * 100' ${OFILE})

#Push all this into a local InfluxDB in a db called MyWeather
curl -XPOST "http://${INFLUX}:${INFLUX_PORT}/write?db=${INFLUX_DB}" --data-binary 'Temperature value='"$TEMP"''
curl -XPOST "http://${INFLUX}:${INFLUX_PORT}/write?db=${INFLUX_DB}" --data-binary 'FeelsLike value='"$FEELSLIKE"''
curl -XPOST "http://${INFLUX}:${INFLUX_PORT}/write?db=${INFLUX_DB}" --data-binary 'Humidity value='"$HUMIDITY"''
