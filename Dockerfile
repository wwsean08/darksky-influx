FROM alpine:3.9
WORKDIR /app
ENTRYPOINT ["/app/darksky.sh"]
RUN apk add -u curl jq bash
COPY darksky.sh ./