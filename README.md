# darksky-influx
This is a script/docker container used to get the current weather from darksky and insert it into influxdb.

## Environment Variables
The following environment variables are used for configuring the application

| Variable      | Description   |
| ------------- |---------------|
| API_KEY       | The API key used to talk to darksky |
| LOCATION      | The location in lat,lon to get the current status for, example `45.6483,-122.3369` |
| INFLUX        | The hostname/IP address of the influx server |
| INFLUX_PORT   | The port to connect to influx on |
| INFLUX_DB     | The DB to store data in (must already exist) |

## Usage
This can be run using a cronjob or a cronjob within kubernetes, below is an example of a kubernetes cronjob to run this ever 5 minutes:

```yaml
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: darksky2influx
spec:
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: darksky2influx
              # This image will work on either AMD64 or ARM
              image: registry.gitlab.com/wwsean08/darksky-influx:v0.0.2
              imagePullPolicy: Always
              env:
                - name: API_KEY
                  valueFrom:
                    secretKeyRef:
                      key: API_KEY
                      name: settings
                - name: LOCATION
                  valueFrom:
                    secretKeyRef:
                      key: LOCATION
                      name: settings
                - name: INFLUX
                  valueFrom:
                    secretKeyRef:
                      key: INFLUX
                      name: settings
                - name: INFLUX_PORT
                  valueFrom:
                    secretKeyRef:
                      key: INFLUX_PORT
                      name: settings
                - name: INFLUX_DB
                  valueFrom:
                    secretKeyRef:
                      key: INFLUX_DB
                      name: settings
          restartPolicy: Never
  schedule: "*/5 * * * *"
```